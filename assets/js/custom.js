"use strict";

jQuery(document).ready(function($) {
  // init sentry error tracking
  Sentry.init({
    dsn: "https://b778c2e8dd764d6faf806698d49a5a9a@sentry.io/1859196"
  });

  // allow WP options to disable certain bag sizes in registration form
  var bagSizeSelect = $("#cleanups-form__bag-size")
    .children("div.acf-input")
    .children("select");
  if (typeof largeBagOrdersEnabled !== "undefined" && !largeBagOrdersEnabled) {
    bagSizeSelect.children('option[value="large"]').remove();
    bagSizeSelect.trigger("change");
  }
  if (typeof smallBagOrdersEnabled !== "undefined" && !smallBagOrdersEnabled) {
    bagSizeSelect.children('option[value="small"]').remove();
    bagSizeSelect.trigger("change");
  }

  var aniSpeed = 10;

  $("#places-form").on("submit", function(e) {
    var inputLat = $("#lat").val();
    var inputLng = $("#lng").val();

    if (inputLat == null || inputLng == null) {
      e.preventDefault();
      setTimeout(function() {
        $("#places-form").trigger("submit");
      }, 100);
    }
  });

  $(".delete-post-button").on("click", function() {
    $(".delete-modal-bg").fadeIn(300);
  });

  $(".nevermind").on("click", function() {
    $(".delete-modal-bg").fadeOut(300);
  });

  $(".menu-main-wrapper .mobile-menu-button").on("click", function() {
    $(".menu-wrap-hide").slideToggle(300);
    $(".sub-menu").hide();
    $(".menu-item-has-children").removeClass("open");
  });

  $(".tab-selector-mobile-menu-button").on("click", function() {
    $(".tab-selector .mobile-wrap").slideToggle();
    $(".tab-selector .mobile-wrap").css("display", "flex");
  });

  $(".menu-item-has-children").on("click", function(e) {
    if (!$(this).hasClass("open") && $(".mobile-menu-button").is(":visible")) {
      e.preventDefault();
      $(this)
        .children(".sub-menu")
        .slideDown(300);
      $(this).addClass("open");
    }
  });

  $("#ajax-attend-buttons").on(
    "click",
    "#attend-button, #unattend-button",
    function() {
      // $('#add').submit();

      var status = $(this).data("status");

      $.ajax({
        type: "POST",
        url: ajaxurl,
        data: {
          action: "add_user_to_cleanup",
          cleanup_id: current_post_id,
          add_remove: status
        },
        success: function success(data) {
          $("#attend-button").remove();
          if (status == "add") {
            $("#ajax-attend-buttons").html(
              "<div id='response'></div><div id='unattend-button' data-status='remove' class='button attend-button'>Cancel my attendance</div>"
            );
          } else if (status == "remove") {
            $("#ajax-attend-buttons").html(
              "<div id='response'></div><div id='attend-button' data-status='add' class='button attend-button'>Attend this cleanup</div>"
            );
          }

          $("#response").html(data);

          $.ajax({
            type: "GET",
            url: ajaxurl,
            data: {
              action: "middle_menu_refresh"
            },
            success: function success(response) {
              $(".tab-selector").remove();
              // console.log(response);
              $(".acf-container").after(response);
            }
          });
        },
        error: function error(err) {
          $("#response").text("error! " + err);
        }
      });
    }
  );

  $(".no-order-menu .close").on("click", function() {
    $.ajax({
      type: "GET",
      url: ajaxurl,
      data: {
        action: "hide_menu_message"
      },
      success: function success(response) {
        $(".no-order-menu").fadeOut(300);

        $.ajax({
          type: "GET",
          url: ajaxurl,
          data: {
            action: "middle_menu_refresh"
          },
          success: function success(response) {
            $(".tab-selector").remove();
            console.log(response);
            $(".acf-container").after(response);
          }
        });
      }
    });
  });

  $("#ajaxfunction").on("click", function() {
    $.ajax({
      type: "GET",
      url: ajaxurl,
      data: {
        action: "emailReminder"
      },
      success: function success(response) {
        console.log(response);
      }
    });
  });

  var formDisable = true;

  function initialize() {
    var input = document.getElementById("search-field");
    var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, "place_changed", function() {
      var place = autocomplete.getPlace();
      document.getElementById("lat").value = place.geometry.location.lat();
      document.getElementById("lng").value = place.geometry.location.lng();
      formDisable = false;
    });
  }

  // if ($('.acf-map')) {
  //   google.maps.event.addDomListener(window, 'load', initialize);
  // }

  $("#places-form").submit(function(e) {
    if (formDisable == true) {
      e.preventDefault();
    }
  });

  if ($(".cleanup-woo-product").length) {
    $("input").attr("checked", false);

    var originalhref = $(".proceed-button").attr("href") + "?add-to-cart=";

    $(".cleanup-woo-product input").on("click", function() {
      var inputs = $(".cleanup-woo-product input:checked");
      var href = "";
      inputs.each(function() {
        href = href + $(this).data("id");
        var $this = $(this);
        if ($this[0] !== inputs.last()[0]) {
          href = href + ",";
        }
      });

      href = originalhref + href;

      $(".proceed-button").attr("href", href);
    });
  }

  $(".scroll-top").on("click", function(e) {
    e.preventDefault();
    $("html, body").animate({ scrollTop: "0px" }, 300);
  });

  // if ($('.acf-map').length) {
  //   console.log('working');
  //   google.maps.event.addListener(infowindow, 'domready', () => {
  //
  //     // Reference to the DIV which receives the contents of the infowindow using jQuery
  //     const iwOuter = $('.gm-style-iw');
  //
  //     /* The DIV we want to change is above the .gm-style-iw DIV.
  //      * So, we use jQuery and create a iwBackground variable,
  //      * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
  //      */
  //     const iwBackground = iwOuter.prev();
  //
  //     //  $(iwBackground).hide();
  //     //
  //     // // Remove the background shadow DIV
  //     //  iwBackground.children('div:nth-child(3)').hide();
  //     iwBackground.children('div:nth-child(3)').css('margin-top', '-21px');
  //     iwBackground.children('div:nth-child(3)').css('margin-top', '-21px');
  //     //
  //     // // Remove the white background DIV
  //     // iwBackground.children(':nth-child(4)').css({'display' : 'none'});
  //   });
  // }
  $(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
      // > 100px from top - show div
      $(".footer-scroll-top").fadeIn(300);
    } else {
      // <= 100px from top - hide div
      $(".footer-scroll-top").fadeOut(300);
    }
  });
});
