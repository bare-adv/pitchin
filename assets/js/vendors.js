"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
 *  Vide - v0.5.0
 *  Easy as hell jQuery plugin for video backgrounds.
 *  http://vodkabears.github.io/vide/
 *
 *  Made by Ilya Makarov
 *  Under MIT License
 */
!function (a, b) {
  "function" == typeof define && define.amd ? define(["jquery"], b) : b("object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? require("jquery") : a.jQuery);
}(undefined, function (a) {
  "use strict";
  function b(a) {
    var b,
        c,
        d,
        e,
        f,
        g,
        h,
        i = {};for (f = a.replace(/\s*:\s*/g, ":").replace(/\s*,\s*/g, ",").split(","), h = 0, g = f.length; g > h && (c = f[h], -1 === c.search(/^(http|https|ftp):\/\//) && -1 !== c.search(":")); h++) {
      b = c.indexOf(":"), d = c.substring(0, b), e = c.substring(b + 1), e || (e = void 0), "string" == typeof e && (e = "true" === e || ("false" === e ? !1 : e)), "string" == typeof e && (e = isNaN(e) ? e : +e), i[d] = e;
    }return null == d && null == e ? a : i;
  }function c(a) {
    a = "" + a;var b,
        c,
        d,
        e = a.split(/\s+/),
        f = "50%",
        g = "50%";for (d = 0, b = e.length; b > d; d++) {
      c = e[d], "left" === c ? f = "0%" : "right" === c ? f = "100%" : "top" === c ? g = "0%" : "bottom" === c ? g = "100%" : "center" === c ? 0 === d ? f = "50%" : g = "50%" : 0 === d ? f = c : g = c;
    }return { x: f, y: g };
  }function d(b, c) {
    var d = function d() {
      c(this.src);
    };a('<img src="' + b + '.gif">').load(d), a('<img src="' + b + '.jpg">').load(d), a('<img src="' + b + '.jpeg">').load(d), a('<img src="' + b + '.png">').load(d);
  }function e(c, d, e) {
    if (this.$element = a(c), "string" == typeof d && (d = b(d)), e ? "string" == typeof e && (e = b(e)) : e = {}, "string" == typeof d) d = d.replace(/\.\w*$/, "");else if ("object" == (typeof d === "undefined" ? "undefined" : _typeof(d))) for (var f in d) {
      d.hasOwnProperty(f) && (d[f] = d[f].replace(/\.\w*$/, ""));
    }this.settings = a.extend({}, g, e), this.path = d;try {
      this.init();
    } catch (i) {
      if (i.message !== h) throw i;
    }
  }var f = "vide",
      g = { volume: 1, playbackRate: 1, muted: !0, loop: !0, autoplay: !0, position: "50% 50%", posterType: "detect", resizing: !0, bgColor: "transparent", className: "" },
      h = "Not implemented";e.prototype.init = function () {
    var b,
        e,
        g = this,
        i = g.path,
        j = i,
        k = "",
        l = g.$element,
        m = g.settings,
        n = c(m.position),
        o = m.posterType;e = g.$wrapper = a("<div>").addClass(m.className).css({ position: "absolute", "z-index": -1, top: 0, left: 0, bottom: 0, right: 0, overflow: "hidden", "-webkit-background-size": "cover", "-moz-background-size": "cover", "-o-background-size": "cover", "background-size": "cover", "background-color": m.bgColor, "background-repeat": "no-repeat", "background-position": n.x + " " + n.y }), "object" == (typeof i === "undefined" ? "undefined" : _typeof(i)) && (i.poster ? j = i.poster : i.mp4 ? j = i.mp4 : i.webm ? j = i.webm : i.ogv && (j = i.ogv)), "detect" === o ? d(j, function (a) {
      e.css("background-image", "url(" + a + ")");
    }) : "none" !== o && e.css("background-image", "url(" + j + "." + o + ")"), "static" === l.css("position") && l.css("position", "relative"), l.prepend(e), "object" == (typeof i === "undefined" ? "undefined" : _typeof(i)) ? (i.mp4 && (k += '<source src="' + i.mp4 + '.mp4" type="video/mp4">'), i.webm && (k += '<source src="' + i.webm + '.webm" type="video/webm">'), i.ogv && (k += '<source src="' + i.ogv + '.ogv" type="video/ogg">'), b = g.$video = a("<video>" + k + "</video>")) : b = g.$video = a('<video><source src="' + i + '.mp4" type="video/mp4"><source src="' + i + '.webm" type="video/webm"><source src="' + i + '.ogv" type="video/ogg"></video>');try {
      b.prop({ autoplay: m.autoplay, loop: m.loop, volume: m.volume, muted: m.muted, defaultMuted: m.muted, playbackRate: m.playbackRate, defaultPlaybackRate: m.playbackRate });
    } catch (p) {
      throw new Error(h);
    }b.css({ margin: "auto", position: "absolute", "z-index": -1, top: n.y, left: n.x, "-webkit-transform": "translate(-" + n.x + ", -" + n.y + ")", "-ms-transform": "translate(-" + n.x + ", -" + n.y + ")", "-moz-transform": "translate(-" + n.x + ", -" + n.y + ")", transform: "translate(-" + n.x + ", -" + n.y + ")", visibility: "hidden", opacity: 0 }).one("canplaythrough." + f, function () {
      g.resize();
    }).one("playing." + f, function () {
      b.css({ visibility: "visible", opacity: 1 }), e.css("background-image", "none");
    }), l.on("resize." + f, function () {
      m.resizing && g.resize();
    }), e.append(b);
  }, e.prototype.getVideoObject = function () {
    return this.$video[0];
  }, e.prototype.resize = function () {
    if (this.$video) {
      var a = this.$wrapper,
          b = this.$video,
          c = b[0],
          d = c.videoHeight,
          e = c.videoWidth,
          f = a.height(),
          g = a.width();g / e > f / d ? b.css({ width: g + 2, height: "auto" }) : b.css({ width: "auto", height: f + 2 });
    }
  }, e.prototype.destroy = function () {
    delete a[f].lookup[this.index], this.$video && this.$video.off(f), this.$element.off(f).removeData(f), this.$wrapper.remove();
  }, a[f] = { lookup: [] }, a.fn[f] = function (b, c) {
    var d;return this.each(function () {
      d = a.data(this, f), d && d.destroy(), d = new e(this, b, c), d.index = a[f].lookup.push(d) - 1, a.data(this, f, d);
    }), this;
  }, a(document).ready(function () {
    var b = a(window);b.on("resize." + f, function () {
      for (var b, c = a[f].lookup.length, d = 0; c > d; d++) {
        b = a[f].lookup[d], b && b.settings.resizing && b.resize();
      }
    }), b.on("unload." + f, function () {
      return !1;
    }), a(document).find("[data-" + f + "-bg]").each(function (b, c) {
      var d = a(c),
          e = d.data(f + "-options"),
          g = d.data(f + "-bg");d[f](g, e);
    });
  });
});

/*!
 * parallax.js v1.4.2 (http://pixelcog.github.io/parallax.js/)
 * @copyright 2016 PixelCog, Inc.
 * @license MIT (https://github.com/pixelcog/parallax.js/blob/master/LICENSE)
 */
!function (t, i, e, s) {
  function o(i, e) {
    var h = this;"object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && (delete e.refresh, delete e.render, t.extend(this, e)), this.$element = t(i), !this.imageSrc && this.$element.is("img") && (this.imageSrc = this.$element.attr("src"));var r = (this.position + "").toLowerCase().match(/\S+/g) || [];if (r.length < 1 && r.push("center"), 1 == r.length && r.push(r[0]), ("top" == r[0] || "bottom" == r[0] || "left" == r[1] || "right" == r[1]) && (r = [r[1], r[0]]), this.positionX != s && (r[0] = this.positionX.toLowerCase()), this.positionY != s && (r[1] = this.positionY.toLowerCase()), h.positionX = r[0], h.positionY = r[1], "left" != this.positionX && "right" != this.positionX && (this.positionX = isNaN(parseInt(this.positionX)) ? "center" : parseInt(this.positionX)), "top" != this.positionY && "bottom" != this.positionY && (this.positionY = isNaN(parseInt(this.positionY)) ? "center" : parseInt(this.positionY)), this.position = this.positionX + (isNaN(this.positionX) ? "" : "px") + " " + this.positionY + (isNaN(this.positionY) ? "" : "px"), navigator.userAgent.match(/(iPod|iPhone|iPad)/)) return this.imageSrc && this.iosFix && !this.$element.is("img") && this.$element.css({ backgroundImage: "url(" + this.imageSrc + ")", backgroundSize: "cover", backgroundPosition: this.position }), this;if (navigator.userAgent.match(/(Android)/)) return this.imageSrc && this.androidFix && !this.$element.is("img") && this.$element.css({ backgroundImage: "url(" + this.imageSrc + ")", backgroundSize: "cover", backgroundPosition: this.position }), this;this.$mirror = t("<div />").prependTo("body");var a = this.$element.find(">.parallax-slider"),
        n = !1;0 == a.length ? this.$slider = t("<img />").prependTo(this.$mirror) : (this.$slider = a.prependTo(this.$mirror), n = !0), this.$mirror.addClass("parallax-mirror").css({ visibility: "hidden", zIndex: this.zIndex, position: "fixed", top: 0, left: 0, overflow: "hidden" }), this.$slider.addClass("parallax-slider").one("load", function () {
      h.naturalHeight && h.naturalWidth || (h.naturalHeight = this.naturalHeight || this.height || 1, h.naturalWidth = this.naturalWidth || this.width || 1), h.aspectRatio = h.naturalWidth / h.naturalHeight, o.isSetup || o.setup(), o.sliders.push(h), o.isFresh = !1, o.requestRender();
    }), n || (this.$slider[0].src = this.imageSrc), (this.naturalHeight && this.naturalWidth || this.$slider[0].complete || a.length > 0) && this.$slider.trigger("load");
  }function h(s) {
    return this.each(function () {
      var h = t(this),
          r = "object" == (typeof s === "undefined" ? "undefined" : _typeof(s)) && s;this == i || this == e || h.is("body") ? o.configure(r) : h.data("px.parallax") ? "object" == (typeof s === "undefined" ? "undefined" : _typeof(s)) && t.extend(h.data("px.parallax"), r) : (r = t.extend({}, h.data(), r), h.data("px.parallax", new o(this, r))), "string" == typeof s && ("destroy" == s ? o.destroy(this) : o[s]());
    });
  }!function () {
    for (var t = 0, e = ["ms", "moz", "webkit", "o"], s = 0; s < e.length && !i.requestAnimationFrame; ++s) {
      i.requestAnimationFrame = i[e[s] + "RequestAnimationFrame"], i.cancelAnimationFrame = i[e[s] + "CancelAnimationFrame"] || i[e[s] + "CancelRequestAnimationFrame"];
    }i.requestAnimationFrame || (i.requestAnimationFrame = function (e) {
      var s = new Date().getTime(),
          o = Math.max(0, 16 - (s - t)),
          h = i.setTimeout(function () {
        e(s + o);
      }, o);return t = s + o, h;
    }), i.cancelAnimationFrame || (i.cancelAnimationFrame = function (t) {
      clearTimeout(t);
    });
  }(), t.extend(o.prototype, { speed: .2, bleed: 0, zIndex: -100, iosFix: !0, androidFix: !0, position: "center", overScrollFix: !1, refresh: function refresh() {
      this.boxWidth = this.$element.outerWidth(), this.boxHeight = this.$element.outerHeight() + 2 * this.bleed, this.boxOffsetTop = this.$element.offset().top - this.bleed, this.boxOffsetLeft = this.$element.offset().left, this.boxOffsetBottom = this.boxOffsetTop + this.boxHeight;var t = o.winHeight,
          i = o.docHeight,
          e = Math.min(this.boxOffsetTop, i - t),
          s = Math.max(this.boxOffsetTop + this.boxHeight - t, 0),
          h = this.boxHeight + (e - s) * (1 - this.speed) | 0,
          r = (this.boxOffsetTop - e) * (1 - this.speed) | 0;if (h * this.aspectRatio >= this.boxWidth) {
        this.imageWidth = h * this.aspectRatio | 0, this.imageHeight = h, this.offsetBaseTop = r;var a = this.imageWidth - this.boxWidth;this.offsetLeft = "left" == this.positionX ? 0 : "right" == this.positionX ? -a : isNaN(this.positionX) ? -a / 2 | 0 : Math.max(this.positionX, -a);
      } else {
        this.imageWidth = this.boxWidth, this.imageHeight = this.boxWidth / this.aspectRatio | 0, this.offsetLeft = 0;var a = this.imageHeight - h;this.offsetBaseTop = "top" == this.positionY ? r : "bottom" == this.positionY ? r - a : isNaN(this.positionY) ? r - a / 2 | 0 : r + Math.max(this.positionY, -a);
      }
    }, render: function render() {
      var t = o.scrollTop,
          i = o.scrollLeft,
          e = this.overScrollFix ? o.overScroll : 0,
          s = t + o.winHeight;this.boxOffsetBottom > t && this.boxOffsetTop <= s ? (this.visibility = "visible", this.mirrorTop = this.boxOffsetTop - t, this.mirrorLeft = this.boxOffsetLeft - i, this.offsetTop = this.offsetBaseTop - this.mirrorTop * (1 - this.speed)) : this.visibility = "hidden", this.$mirror.css({ transform: "translate3d(0px, 0px, 0px)", visibility: this.visibility, top: this.mirrorTop - e, left: this.mirrorLeft, height: this.boxHeight, width: this.boxWidth }), this.$slider.css({ transform: "translate3d(0px, 0px, 0px)", position: "absolute", top: this.offsetTop, left: this.offsetLeft, height: this.imageHeight, width: this.imageWidth, maxWidth: "none" });
    } }), t.extend(o, { scrollTop: 0, scrollLeft: 0, winHeight: 0, winWidth: 0, docHeight: 1 << 30, docWidth: 1 << 30, sliders: [], isReady: !1, isFresh: !1, isBusy: !1, setup: function setup() {
      if (!this.isReady) {
        var s = t(e),
            h = t(i),
            r = function r() {
          o.winHeight = h.height(), o.winWidth = h.width(), o.docHeight = s.height(), o.docWidth = s.width();
        },
            a = function a() {
          var t = h.scrollTop(),
              i = o.docHeight - o.winHeight,
              e = o.docWidth - o.winWidth;o.scrollTop = Math.max(0, Math.min(i, t)), o.scrollLeft = Math.max(0, Math.min(e, h.scrollLeft())), o.overScroll = Math.max(t - i, Math.min(t, 0));
        };h.on("resize.px.parallax load.px.parallax", function () {
          r(), o.isFresh = !1, o.requestRender();
        }).on("scroll.px.parallax load.px.parallax", function () {
          a(), o.requestRender();
        }), r(), a(), this.isReady = !0;
      }
    }, configure: function configure(i) {
      "object" == (typeof i === "undefined" ? "undefined" : _typeof(i)) && (delete i.refresh, delete i.render, t.extend(this.prototype, i));
    }, refresh: function refresh() {
      t.each(this.sliders, function () {
        this.refresh();
      }), this.isFresh = !0;
    }, render: function render() {
      this.isFresh || this.refresh(), t.each(this.sliders, function () {
        this.render();
      });
    }, requestRender: function requestRender() {
      var t = this;this.isBusy || (this.isBusy = !0, i.requestAnimationFrame(function () {
        t.render(), t.isBusy = !1;
      }));
    }, destroy: function destroy(e) {
      var s,
          h = t(e).data("px.parallax");for (h.$mirror.remove(), s = 0; s < this.sliders.length; s += 1) {
        this.sliders[s] == h && this.sliders.splice(s, 1);
      }t(e).data("px.parallax", !1), 0 === this.sliders.length && (t(i).off("scroll.px.parallax resize.px.parallax load.px.parallax"), this.isReady = !1, o.isSetup = !1);
    } });var r = t.fn.parallax;t.fn.parallax = h, t.fn.parallax.Constructor = o, t.fn.parallax.noConflict = function () {
    return t.fn.parallax = r, this;
  }, t(e).on("ready.px.parallax.data-api", function () {
    t('[data-parallax="scroll"]').parallax();
  });
}(jQuery, window, document);