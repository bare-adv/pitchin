<?php if( have_rows( 'content' ) ): ?>
  <section class="main-flexible-content">
    <?php $flexibleBaseUrl = get_stylesheet_directory() . '/template-parts/flexible-content/'; ?>
    <?php while( have_rows( 'content' ) ): the_row(); ?>
      <?php $row_layout = get_row_layout(); ?>
      <?php include( $flexibleBaseUrl . $row_layout . '.php' ); ?>
    <?php endwhile; ?>

  </section>
<?php endif; ?>
