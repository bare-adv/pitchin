<?php

$field_groups = [12612];
if (ICL_LANGUAGE_CODE == 'en'):
  $field_groups = [12612];
elseif (ICL_LANGUAGE_CODE == 'fr'):
  $field_groups = [12638];
endif;

$post_id = get_the_ID();
$redirect_page_link = '/pitch-in-week/my-cleanups/';

$options = array(
'id' => 'register-cleanup',
'post_title' => false,
'return' => '%post_url%?updated=true',
'field_groups' => $field_groups,
'post_id'		=> $post_id,
'new_post'		=> array(
  'post_type'		=> 'cleanups',
  'post_status'		=> 'publish'
),
'submit_value'		=> 'Submit',
'return' => $redirect_page_link
);

?>

<h1>Editing <?php the_title(); ?></h1>
<?php acf_form($options); ?>