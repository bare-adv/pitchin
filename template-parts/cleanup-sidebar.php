<aside class='cleanup-sidebar'>
  <h3>Search by location</h3>
  <form role="search" action="<?php echo site_url('/all-cleanups/'); ?>" method="get" id="places-form">
    <input id="search-field" type="text" size="50">
    <input type="hidden" name='lng' id='lng'>
    <input type="hidden" name='lat' id='lat'>
    <input type="submit" id='places-submit' alt="Search" class='button attend-button' value="Search" />
  </form>

  <!-- <h3>Search Cleanups</h3>
  <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
    <input type="text" name="s" placeholder="Search Cleanups"/>
    <input type="hidden" name="post_type" value="cleanups" />
    <input type="submit" alt="Search" class='button attend-button' value="Search" />
  </form> -->
</aside>
