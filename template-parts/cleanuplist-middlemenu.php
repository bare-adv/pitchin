<?php
$getdate = getdate();
$state = array(
  'user_id' => get_current_user_id(),
  'page_id' => get_the_ID(),
  'logged_in' => is_user_logged_in(),
  'page_year' => get_query_var('yr'),
  'this_year' => $getdate['year'],
  'last_year' => ($getdate['year'] - 1)
);
$state['template'] = get_post_meta( $state['page_id'], '_wp_page_template', true );
if ($state['logged_in']):
  $state['cleanup_count'] = count_user_posts($state['user_id'], 'cleanups');
endif;
?>

<section class="tab-selector scope-selector">
    <div class="grid mobile-wrap">
        <a href='<?php the_permalink(703); ?>' class="
    tab-selector-item
    <?php if(($state['page_id'] == 5516) && ($state['page_year'] != $state['last_year'])): ?>
      active
    <?php endif; ?>">
            <?php echo $state['this_year']; ?> Cleanups
        </a>
        <a href='<?php the_permalink(703); ?>?yr=<?php echo $state['last_year']; ?>' class="
    tab-selector-item
    <?php if(($state['page_id'] == 1312) && ($state['page_year'] == $state['last_year'])): ?>
      active
    <?php endif; ?>
    ">
            <?php echo $state['last_year']; ?> Cleanups
        </a>
        <?php if($state['logged_in']): ?>
        <?php if ($state['cleanup_count'] > 0): //if user has created cleanups?>
        <a href='<?php the_permalink(959); ?>' class="
            tab-selector-item
            <?php if($state['page_id'] == 5625): ?>
              active
            <?php endif; ?>
            ">
            My Cleanups
        </a>
        <?php endif; ?>

        <?php if($state['cleanup_count'] == 0): ?>
        <a href='<?php the_permalink(885); ?>' class="tab-selector-item-highlight">Register Clean-up</a>
        <?php else: ?>
        <div class="tab-selector-item-plus">+
            <ul class="tab-dropdown">
                <li><a href="<?php the_permalink(885);?>" class='tab-selector-item'>New Cleanup</a></li>
            </ul>
        </div>


        <a class='mobile-only tab-selector-item-highlight' href="<?php the_permalink(885);?>"
            class='tab-selector-item'>New Cleanup</a>

        <?php endif; ?>
        <!-- </div> -->

        <?php elseif(!is_user_logged_in()): ?>
        <a href='<?php echo get_permalink(12514); ?>'
            class="tab-selector-item-highlight tab-selector-item-plus-two">Sign up</a>
        <a href='<?php echo get_permalink(12514); ?>' class="tab-selector-item-highlight">Log in</a>
        <?php endif; ?>

        <div class="right">
            <?php //if($cleanups && is_object($cleanups)): ?>
            <!-- <?php //echo $cleanups->post_count; ?> Cleanups -->
            <?php //endif; ?>
            <div class="button" id='locate-me'><?= translateACF('locate_me'); ?></div>
            <div class="button" id='zoom-out'><?= translateACF('zoom_out'); ?></div>
        </div>
    </div>
    <div class="tab-selector-mobile-menu-button">
        <span class="lines"></span>
    </div>
</section>