<?php

$fields = array(
  'group-description',
  'contact_information',
  'group',
);
$options = array(
  'post_title' => true,
  'return' => '%post_url%?updated=true',
  'fields' => $fields,
); ?>

<h1>Editing Group <?php the_title(); ?></h1>
<?php acf_form($options); ?>
