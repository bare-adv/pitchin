<div class="grid">
  <?php if (have_posts()) : ?>
    <section class="posts section">
      <?php while (have_posts()) : the_post(); ?>

        <div class="post">
          <h2><?php the_title(); ?></h2>
          <?php if(has_post_thumbnail()): ?>
            <?php $thumb_id = get_post_thumbnail_id(); ?>
            <?php $thumb_url = wp_get_attachment_image_src($thumb_id,'medium', true); ?>
            <img src="<?php echo $thumb_url[0]; ?>" alt="<?php the_title(); ?>">
          <?php endif; ?>
          <?php the_excerpt(); ?>
          <a href="<?php the_permalink(); ?>" class="button attend-button">Read more</a>
        </div>

    	<?php endwhile; ?>
    </section>

  		<?php echo custom_pagination(); ?>

  	<?php else : ?>

  		<?php // No Posts Found ?>

  <?php endif; ?>
</div>
