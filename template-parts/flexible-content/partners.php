<div class="grid section">
  <?php if ( have_rows('partners_section') ): ?>
    <section class="faq">
      <?php while ( have_rows('partners_section') ) : the_row(); ?>
        <div class="faq__row">
          <h2 class='faq__q'>
            <?php the_sub_field('section_title'); ?>
          </h2>
          <div class='faq__a'>
            <?php if ( have_rows('partners') ): ?>
              <div class="partners">
                <?php while ( have_rows('partners') ) : the_row(); ?>
                  <?php $link = get_sub_field('link'); ?>
                  <?php $image = get_sub_field('image'); ?>
                  <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target'] ?>">
                    <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['filename']; ?>">
                  </a>
                <?php endwhile; ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
      <?php endwhile; ?>
    </section>
  <?php endif; ?>
</div>
