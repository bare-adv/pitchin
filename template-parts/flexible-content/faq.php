<div class="grid section">
  <?php if ( have_rows('faq') ): ?>
    <section class="faq">
      <?php while ( have_rows('faq') ) : the_row(); ?>
        <div class="faq__row">
          <h2 class='faq__q'>
            <?php the_sub_field('question'); ?>
          </h2>
          <div class='faq__a'>
            <?php the_sub_field('answer'); ?>
          </div>
        </div>
      <?php endwhile; ?>
    </section>
  <?php endif; ?>
</div>
