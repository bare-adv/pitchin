"use strict";

//
// Project variables
//
const project = "Pitch-in", // Project Name.
  projectURL = "pitchin.local"; // Project URL. Could be something like localhost:8888.

//
// Project directories
//
const styleWatchFiles = ["src/smacss/**/*.scss"], // Path to all *.scss files inside css folder and inside them.
  ggfConfig = "src/fonts.neon",
  ggfOutputDir = "assets/typography/base64-fonts",
  customJSWatchFiles = "src/js/*.js", // Path to all custom JS files.
  jsDist = "assets/**/*.js",
  projectPHPWatchFiles = "**/*.php", // Path to all PHP files.
  //
  // Dependencies and methods
  //
  gulp = require("gulp"),
  sass = require("gulp-sass"),
  sourcemaps = require("gulp-sourcemaps"),
  autoprefixer = require("gulp-autoprefixer"),
  importer = require("node-sass-globbing"),
  plumber = require("gulp-plumber"),
  browserSync = require("browser-sync").create(),
  ggf = require("gulp-google-fonts"),
  babel = require("gulp-babel"),
  glebab = require("gulp-lebab"),
  print = require("gulp-print"),
  exec = require("exec-chainable"),
  gutil = require("gulp-util"),
  postcss = require("gulp-postcss"),
  reload = browserSync.reload; // For manual browser reload.

//
// Browserlist https://github.com/ai/browserslist
//
const AUTOPREFIXER_BROWSERS = [
  "last 2 version",
  "> 1%",
  "ie >= 11",
  "ff >= 30",
  "chrome >= 34",
  "safari >= 7",
  "opera >= 23",
  "ios >= 7",
  "android >= 4",
];

//
// Sass tools
//
var sass_config = {
  importer: importer,
  includePaths: [
    "node_modules/modularscale-sass/stylesheets",
    "node_modules/compass-mixins/lib/",
    "node_modules/susy/sass",
  ],
};

//
// BrowserSync configuration
//
gulp.task("browser-sync", function () {
  browserSync.init({
    proxy: projectURL,
    open: true,
    injectChanges: true,
    notify: false,
  });
});

//
//  Typographyjs
//
const typography = require("postcss-typography"),
  options = {
    baseFontSize: "1px",
    baseLineHeight: 1.45,
    headerFontFamily: ["sans-serif", "Helvetica Neue", "Helvetica", "Arial"],
    bodyFontFamily: ["sans-serif", "Helvetica Neue", "Helvetica", "Arial"],
  },
  postCssPlugins = [typography(options)];

gulp.task("typographyjs", function () {
  return gulp
    .src("./style.css")
    .pipe(postcss(postCssPlugins))
    .pipe(gulp.dest("./"));
});

//
//  scss transpilation and injection
//
gulp.task("sass", function () {
  return gulp
    .src(styleWatchFiles)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass(sass_config).on("error", sass.logError))
    .pipe(autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe(
      sourcemaps.write("", {
        addComment: true,
      })
    )
    .pipe(gulp.dest(""))
    .pipe(browserSync.stream());
});

//
// Download google fonts and generate css file.
//

gulp.task("getFonts", function () {
  return gulp.src(ggfConfig).pipe(ggf()).pipe(gulp.dest(ggfOutputDir));
});

gulp.task("babel", function () {
  gulp
    .src(customJSWatchFiles)
    .pipe(
      babel({
        presets: ["env"],
      })
    )
    .pipe(gulp.dest("assets/js"));
});

gulp.task("setup", function () {
  return gulp
    .src([jsDist, "!assets/js/vendor/**/*.js"])
    .pipe(print())
    .pipe(glebab())
    .on("error", gutil.log)
    .pipe(gulp.dest("src"));
});

gulp.task("pulldb", function () {
  exec("wordmove pull -d").then((message) => {
    console.log(message);
  });
});

gulp.task("wordmoveInit", function () {
  exec("wordmove init").then((message) => {
    console.log(message);
  });
});

//
// Watch files
//
gulp.task("default", ["browser-sync"], function () {
  gulp.watch(projectPHPWatchFiles, reload);
  gulp.watch(styleWatchFiles, ["sass"]);
  gulp.watch(customJSWatchFiles, ["babel", reload]);
});
