<?php
/**
 * Index File
 */

get_header();

?>
<?php $latest_post = new WP_Query(array(
  'posts_per_page' => 1
)) ?>
<?php if ($latest_post->have_posts()) : ?>

	<?php while ($latest_post->have_posts()) : $latest_post->the_post(); ?>
    <?php if(has_post_thumbnail()): ?>
      <?php $thumb_id = get_post_thumbnail_id(); ?>
      <?php $thumb_url = wp_get_attachment_image_src($thumb_id,'large', true); ?>
    <?php endif; ?>

    <header class='header-main header-blog'
    style="
    background-image: url('<?php echo $thumb_url[0]; ?>');
    background-size: cover;
    background-position: center top;">
      <div class="bg-overlay">

        <div class="inner grid">
          <div class="text">
            <h1><?php the_title(); ?></h1>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="button attend-button" style='margin-left: 0;'>Read more</a>
          </div>
        </div>

      </div>
    </header>

	<?php endwhile; ?>

<?php endif; ?>

<main class='main-content section'>
  <div class="grid">
    <?php $loop = new WP_Query(array(
      'offset' => 1
    )) ?>
    <?php if ($loop->have_posts()) : ?>
      <section class="posts section">
        <?php while ($loop->have_posts()) : $loop->the_post(); ?>

          <div class="post">
            <h2><?php the_title(); ?></h2>
            <?php if(has_post_thumbnail()): ?>
              <?php $thumb_id = get_post_thumbnail_id(); ?>
              <?php $thumb_url = wp_get_attachment_image_src($thumb_id,'medium', true); ?>
              <img src="<?php echo $thumb_url[0]; ?>" alt="<?php the_title(); ?>">
            <?php endif; ?>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="button attend-button">Read more</a>
          </div>

      	<?php endwhile; ?>
      </section>

    		<?php echo custom_pagination(); ?>

    	<?php else : ?>

    		<?php // No Posts Found ?>

    <?php endif; ?>
  </div>

</main>

<?php get_footer(); ?>
