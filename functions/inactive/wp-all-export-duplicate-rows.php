<?php

function array_insert(&$array, $position, $insert)
{
    if (is_int($position)) {
        array_splice($array, $position, 0, $insert);
    } else {
        $pos   = array_search($position, array_keys($array));
        $array = array_merge(
            array_slice($array, 0, $pos),
            $insert,
            array_slice($array, $pos)
        );
    }
}
//
// add_filter('wp_all_export_csv_rows', 'wpai_wp_all_export_csv_rows', 10, 3);
// function wpai_wp_all_export_csv_rows( $articles, $options, $export_id ) {
//
//        if ( ! empty($articles)) {
//                foreach( $articles as $key=>$article ) {
//                  if ($article['Package Name'] === 'More than 900 people (200 bags / 1 box)') {
//                    $new_article = array();
//                    foreach ($article as $header => $value ) {
//                      $new_article[$header] = 'NEW ' . $value;
//                    }
//                    array_insert($articles, $key, $new_article);
//                  }
//                  // $articles[] = $new_article;
//
//                }
//        }
//        return $articles;
// }
add_filter('wp_all_export_csv_rows', 'wpai_wp_all_export_csv_rows', 10, 3);
function wpai_wp_all_export_csv_rows( $articles, $options, $export_id ) {
       $new_article = array();
       if ( ! empty($articles)) {
               foreach( $articles as $key => $article ) {
                       foreach ($article as $header => $value ) {
                               $new_article[$header] = $key . 'NEW ' . $value;

                       }
               }
               $articles[] = $new_article;
       }
       return $articles;
}

 ?>
