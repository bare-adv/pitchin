<?php

add_action('init', 'admin_only');

function admin_only() {

    if( !is_admin() ) {
      return;
    }

    // filter assignemnts and such go here

   $args = array(
     'posts_per_page' => -1,
     'post_type' => 'cleanups',
     'tax_query' => array(
       array(
         'taxonomy' => 'hide_cleanup',
         'field' => 'slug',
         'terms' => 'yes',
         'operator' => 'NOT IN',
       )
     ),
     'date_query' => array(
         array(
             'year'  => date('Y')
         ),
     ),
   );
   $custom_query = new WP_Query( $args );

   if ( $custom_query->have_posts() ) {

     $countAttendees = 0;
     $countbags = 0;
     $box_weight = get_field('number_of_bags', 5908);

     while ( $custom_query->have_posts() ) {
       $custom_query->the_post();

      $attendees = get_field('number_of_attendees', get_the_ID());
      $countAttendees = $countAttendees + (int)$attendees;


      $cleanup_package = get_field('cleanup_package', get_the_ID());

      $value = 0;

      $value = get_field('number_of_bags', $cleanup_package);

      if($value && $value === $box_weight) {
        $total_participants = get_field('number_of_participants_break_to_boxes', get_the_ID());

        $unrounded_total = ($total_participants + 500) / 1000;

        $total = round($unrounded_total);

        $num_bags = ($total * 3) * 200;

        $countbags = $countbags + $num_bags;
    } else {
      $countbags = $countbags + $value;
    }

   }
   error_log($countbags);
   /* Restore original Post Data */
   wp_reset_postdata();
}

}

 ?>
