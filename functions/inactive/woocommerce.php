<?php
add_filter ('woocommerce_add_to_cart_redirect', 'redirect_to_checkout_page');

function redirect_to_checkout_page() {

	global $woocommerce;

	$checkout_url = $woocommerce->cart->get_checkout_url();
	return $checkout_url;
}


//after woo order, add order to all users cleanups order field that don't already have orders
add_action('woocommerce_payment_complete', 'custom_process_order', 10, 1);
function custom_process_order($order_id) {
    $order = new WC_Order( $order_id );
    $user_id = (int)$order->user_id;

    //get all user cleanups
    //for each cleanup check if the cleanup has an order attached
    //if not then use update_field to add the cleanup id to it.


    $cleanups = get_posts(array(
      'post_type' => 'cleanups',
      'author' => $user_id
    ));

    foreach($cleanups as $cleanup) {
      if (!get_field('orders', $cleanup->ID)) {
        update_field('field_58426c551c0d6', $order_id, $cleanup->ID);
      }
    }


}


function woocommerce_maybe_add_multiple_products_to_cart() {
	// Make sure WC is installed, and add-to-cart qauery arg exists, and contains at least one comma.
	if ( ! class_exists( 'WC_Form_Handler' ) || empty( $_REQUEST['add-to-cart'] ) || false === strpos( $_REQUEST['add-to-cart'], ',' ) ) {
		return;
	}

	// Remove WooCommerce's hook, as it's useless (doesn't handle multiple products).
	remove_action( 'wp_loaded', array( 'WC_Form_Handler', 'add_to_cart_action' ), 20 );

	$product_ids = explode( ',', $_REQUEST['add-to-cart'] );
	$count       = count( $product_ids );
	$number      = 0;

	foreach ( $product_ids as $product_id ) {
		if ( ++$number === $count ) {
			// Ok, final item, let's send it back to woocommerce's add_to_cart_action method for handling.
			$_REQUEST['add-to-cart'] = $product_id;

			return WC_Form_Handler::add_to_cart_action();
		}

		$product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $product_id ) );
		$was_added_to_cart = false;
		$adding_to_cart    = wc_get_product( $product_id );

		if ( ! $adding_to_cart ) {
			continue;
		}

		$add_to_cart_handler = apply_filters( 'woocommerce_add_to_cart_handler', $adding_to_cart->product_type, $adding_to_cart );

		/*
		 * Sorry.. if you want non-simple products, you're on your own.
		 *
		 * Related: WooCommerce has set the following methods as private:
		 * WC_Form_Handler::add_to_cart_handler_variable(),
		 * WC_Form_Handler::add_to_cart_handler_grouped(),
		 * WC_Form_Handler::add_to_cart_handler_simple()
		 *
		 * Why you gotta be like that WooCommerce?
		 */
		if ( 'simple' !== $add_to_cart_handler ) {
			continue;
		}

		// For now, quantity applies to all products.. This could be changed easily enough, but I didn't need this feature.
		$quantity          = empty( $_REQUEST['quantity'] ) ? 1 : wc_stock_amount( $_REQUEST['quantity'] );
		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

		if ( $passed_validation && false !== WC()->cart->add_to_cart( $product_id, $quantity ) ) {
			wc_add_to_cart_message( array( $product_id => $quantity ), true );
		}
	}
}

// Fire before the WC_Form_Handler::add_to_cart_action callback.
add_action( 'wp_loaded', 'woocommerce_maybe_add_multiple_products_to_cart', 15 );


add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

function custom_pre_get_posts_query( $q ) {

	if ( ! $q->is_main_query() ) return;
	if ( ! $q->is_post_type_archive() ) return;

	if ( ! is_admin() && is_shop() ) {

		$q->set( 'tax_query', array(array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => array( 'cleanup' ), // Don't display products in the knives category on the shop page
			'operator' => 'NOT IN'
		)));

	}

	remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

}

add_filter('woocommerce_checkout_fields', 'custom_woocommerce_checkout_fields');

function custom_woocommerce_checkout_fields( $fields ) {

     $fields['order']['order_comments']['placeholder'] = 'Notes about your order, e.g. special notes for delivery. "Leave in front of building", etc.';

     return $fields;
}

 ?>
