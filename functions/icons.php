<?php

$baseUrl = get_stylesheet_directory() . '/assets/icons/';


//
// Material Design Icons
//
function material_icon($iconClass) {
  global $baseUrl;
  echo file_get_contents($baseUrl . 'material-design-icons/svg/' . $iconClass . '.svg');
};

function mdi($iconClass) {// material_icon($iconClass) alias
  material_icon($iconClass);
};

function get_material_icon($iconClass) {
  global $baseUrl;
  return file_get_contents($baseUrl . 'material-design-icons/svg/' . $iconClass . '.svg');
};

function get_mdi($iconClass) {// material_icon($iconClass) alias
  get_material_icon($iconClass);
};

//
// Simple Line Icons
//
function simple_line_icon($iconClass) {
  global $baseUrl;
  echo file_get_contents($baseUrl . 'simple-line-icons/' . $iconClass . '.svg');
};

function sli($iconClass) { // simple_line_icon($iconClass) alias
  simple_line_icon($iconClass);
};

function get_simple_line_icon($iconClass) {
  global $baseUrl;
  return file_get_contents($baseUrl . 'simple-line-icons/' . $iconClass . '.svg');
};

function get_sli($iconClass) { // simple_line_icon($iconClass) alias
  get_simple_line_icon($iconClass);
};

//
// Font awesome
//
function font_awesome_icon($iconClass) {
  global $baseUrl;
  echo file_get_contents($baseUrl . 'font-awesome/' . $iconClass . '.svg');
};

function fa($iconClass) {// font_awesome_icon($iconClass) alias
  font_awesome_icon($iconClass);
};

function get_font_awesome_icon($iconClass) {
  global $baseUrl;
  return file_get_contents($baseUrl . 'font-awesome-icons/' . $iconClass . '.svg');
};

function get_fai($iconClass) {// font_awesome_icon($iconClass) alias
  get_font_awesome_icon($iconClass);
};

 ?>
