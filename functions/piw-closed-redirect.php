<?php

function isPIWClosed($redUrl = '') {

  if (get_field('disable_registration', 'option')) {

    if(is_user_logged_in() && current_user_can('administrator')) {
      return;
    } else {
      wp_logout();
      return;
    }

    // if user is not logged in redirect them
    if ($redUrl == '') {
      $redUrl = get_site_url(null, '/pitch-in-week/registration-closed');
    }

    wp_redirect( $redUrl  );
    exit;
  }

}

?>
