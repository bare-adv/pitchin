<?php 

function custom_language_switcher(){
    // $languages = icl_get_languages('skip_missing=0&orderby=code&order=ASC');
    $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
    $fr_exists = false;

    if(!empty($languages)){  
        foreach($languages as $l){

            if(isset($l['code']) && $l['code'] === 'fr') {
                $fr_exists = true;
            };

            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            if ($l['active']) echo '<div class="lang__current">';
            echo $l['language_code'];
            if ($l['active']) echo '</div>';
            if (!$l['active']) echo '</a>';
        }
        
        if (!$fr_exists && ICL_LANGUAGE_CODE != 'fr') {
            echo '<a href="/fr">FR</a>';
        }

        // if no languages found then we fallback to hardcoded language switcher
    } elseif (empty($languages)) {
        if (ICL_LANGUAGE_CODE == 'fr') {
            echo '<a href="/">EN</a>';
            echo '<div class="lang__current">FR</div>';
        } elseif (ICL_LANGUAGE_CODE == 'en') {
            echo '<div class="lang__current">EN</div>';
            echo '<a href="/fr">FR</a>';
        }
    }

}