<?php

// Remove editor in favour of ACF editor
add_action('init', 'remove_support');

function remove_support() {
  remove_post_type_support( 'template_objects', 'editor');
  remove_post_type_support( 'page', 'editor');
}
