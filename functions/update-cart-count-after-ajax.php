<?php
add_filter( 'woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1 );
 
function iconic_cart_count_fragments( $fragments ) {
    
    $fragments['div.cart-count'] = '<div class="cart-count">' . WC()->cart->get_cart_contents_count() . '</div>';
    
    return $fragments;
    
}