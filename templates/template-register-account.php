<?php
/**
 * Template Name: Register Account
 */
isPIWClosed();
get_header();

?>

<main class='main-content section'>
  <?php if(is_home()): ?>
    <?php include(get_stylesheet_directory() . '/template-parts/post-list.php'); ?>
  <?php else: ?>
    <?php include(get_stylesheet_directory() . '/template-parts/flexible-content.php'); ?>

    <?php if (have_posts()) : ?>
    	<?php while (have_posts()) : the_post(); ?>
        <div class="grid">
          <?php the_content(); ?>
        </div>
    	<?php endwhile; ?>
    		<?php // Navigation ?>
    	<?php else : ?>
    		<?php // No Posts Found ?>
    <?php endif; ?>

  <?php endif; ?>
</main>

<?php get_footer(); ?>
