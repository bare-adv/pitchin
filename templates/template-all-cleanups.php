<?php
/**
 * Template Name: All Current Cleanups
 */
 ?>

<?php get_header(); ?>


<main class='main-content mrg-b-0'>

    <?php
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$user_id = get_current_user_id();
  $getdate = getdate();
  $page_year = get_query_var('yr');
  if($page_year) {
    $loop_year = $page_year;
  } else {
    $loop_year = $getdate["year"];
  }
	$args = array(
		'post_type' => 'cleanups',
		'posts_per_page' => -1,
    'tax_query' => array(
      array(
        'taxonomy' => 'hide_cleanup',
        'field' => 'slug',
        'terms' => 'yes',
        'operator' => 'NOT IN',
      )
    ),
    'date_query' => array(
        array(
            'year'  => $loop_year
        ),
    ),
	);

	$cleanups = new WP_Query( $args ); ?>

    <?php if ($cleanups->have_posts()): ?>
    <div class="acf-container full-height-map">
        <section class="acf-map">
            <div style="display: none;">
                <?php while ($cleanups->have_posts()): $cleanups->the_post(); ?>
                <?php
          $location = get_field('address');
          ?>

                <div class="marker" data-lat="<?php echo $location['lat']; ?>"
                    data-lng="<?php echo $location['lng']; ?>">
                    <div class="content">
                        <div class="title col">
                            <h2><?php the_title(); ?></h2>
                            <p class="address"><?php echo $location['address']; ?></p>
                            <a href="<?php the_permalink(); ?>" class="button cleanup-button">View Cleanup</a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </section>
        <div class="loading-animation">
            <div class="uil-poi-css" style="transform:scale(0.6);">
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php include(get_stylesheet_directory() . '/template-parts/cleanuplist-middlemenu.php'); ?>


    <?php

  $args = array(
    'post_type' => 'cleanups',
    'posts_per_page' => -1,
    'author' => $user_id,
    'date_query' => array(
        array(
            'year'  => date('Y')
        ),
    ),
  );
  $yearloop = new WP_Query($args);
  $cleanup_count = $yearloop->post_count;

   ?>
    <?php if( $cleanup_count === 0 ): ?>
    <div class="delete-modal-bg">
        <div class="delete-modal">
            <h2><?= translateACF('havent_registered_cleanups'); ?></h2>
            <p>
                <?= translateACF('register_now'); ?>
            </p>
            <div class="button attend-button nevermind" id='close-modal'><?= translateACF('button_not_now'); ?></div>
            <a href="<?php echo get_site_url(null, '/pitch-in-week/register-cleanup') ?>"
                class="button attend-button"><?= translateACF('button_register_now'); ?></a>
        </div>
    </div>
    <?php endif; ?>

</main>

<?php get_footer(); ?>