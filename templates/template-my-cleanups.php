<?php
/**
 * Template Name: My Cleanups Overview Template
 */

get_header();

?>
<?php $user_id = get_current_user_id(); ?>

<?php $getdate = getdate(); ?>
<main class='main-content mrg-b-0'>
		<?php
		$args = array(
			'post_type' => 'cleanups',
			'author' => $user_id,
      'tax_query' => array(
        array(
          'taxonomy' => 'hide_cleanup',
          'field' => 'slug',
          'terms' => 'yes',
          'operator' => 'NOT IN',
        )
      ),
      'date_query' => array(
          array(
              'year'  => $getdate["year"]
          ),
        )
		);

		$cleanups = new WP_Query( $args ); ?>

    <?php if ($cleanups->have_posts()): ?>
    		<div class="acf-container full-height-map">
    			<section class="acf-map">
            <div style="display: none;">
              <?php while ($cleanups->have_posts()): $cleanups->the_post(); ?>
                <?php if(!get_field('hide_on_frontend')): ?>
                  <?php cleanupPinList(get_the_id()); ?>
                <?php endif; ?>
              <?php endwhile; ?>
            </div>
          </section>
    			<div class="loading-animation">
            <div class="uil-poi-css" style="transform:scale(0.6);">
            </div>
          </div>
    		</div>
      <?php endif; ?>
<?php include(get_stylesheet_directory() . '/template-parts/cleanuplist-middlemenu.php'); ?>
</main>

<?php get_footer(); ?>
