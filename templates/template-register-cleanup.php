<?php
/**
 * Template Name: Register Cleanup template
 */
isPIWClosed();
acf_form_head();
get_header();

$post_id = get_the_id();
// $post = get_post($post_id);
$location = get_field('address', $post_id);
$user_id = get_current_user_id();
$urlvar = get_query_var( 'state' );

$args = array(
  'post_type' => 'cleanups',
  'posts_per_page' => -1,
  'author' => $user_id,
  'date_query' => array(
      array(
          'year'  => date('Y')
      ),
  ),
);
$yearloop = new WP_Query($args);
$cleanup_count = $yearloop->post_count;
// $group_count = count_user_posts( $user_id, 'groups' );

//$cleanups = new WP_Query( $args ); ?>

<link rel="stylesheet" type="text/css"
    href="https://ws1.postescanada-canadapost.ca/css/addresscomplete-2.30.min.css?key=zc83-jb94-yt95-gt15" />
<script type="text/javascript"
    src="https://ws1.postescanada-canadapost.ca/js/addresscomplete-2.30.min.js?key=zc83-jb94-yt95-gt15"></script>


<?php 
  $large_bags_enabled = get_field('large_bag_orders_enabled', 'option');
  $small_bags_enabled = get_field('small_bag_orders_enabled', 'option');
?>

<script>
var largeBagOrdersEnabled = Boolean(`<?= $large_bags_enabled ?>`);
var smallBagOrdersEnabled = Boolean(`<?= $small_bags_enabled ?>`);
</script>

<main class='main-content'>
    <?php include(get_stylesheet_directory() . '/template-parts/flexible-content.php'); ?>


    <div class="content grid">
        <?php
		add_filter( 'acf/get_valid_field', 'change_input_labels');
		function change_input_labels($field) {

			if($field['name'] == '_post_title') {
				$field['label'] = 'Your cleanup title';
			}

			return $field;

		}

    // $redirect_page_link = get_site_url(null, '/pitch-in-week/registration-complete');
    $redirect_page_link = get_permalink(5514);
    

    $field_groups = [12612];
      if (ICL_LANGUAGE_CODE == 'en'):
        $field_groups = [12612];
      elseif (ICL_LANGUAGE_CODE == 'fr'):
        $field_groups = [12638];
      endif;

    $options = array(
      'id' => 'register-cleanup',
      'post_title' => false,
      'return' => '%post_url%?updated=true',
      'field_groups' => $field_groups,
      'post_id'		=> 'new_post',
      'new_post'		=> array(
        'post_type'		=> 'cleanups',
        'post_status'		=> 'publish'
      ),
      'submit_value'		=> 'Submit',
			'return' => $redirect_page_link
    );
     ?>

        <?php acf_register_form( $options ); ?>

        <?php /* The loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>

        <?php if(!get_field('disable_registration', 'option') && $cleanup_count > get_field('max_cleanups', 'option') && !current_user_can('administrator')): ?>
        <h1><?= translateACF('cleanup_limit_reached'); ?> (<?php echo $cleanup_count; ?> Clean-ups)</h1>
        <p>
            <?= translateACF('need_register_more_cleanups'); ?>
        </p>
        <div class="row">
            <br>
            <a class='button attend-button'
                href="<?php the_permalink(77); ?>"><?= translateACF('contact_administrator'); ?></a>
        </div>
        <?php elseif(get_field('disable_registration', 'option')): ?>
        <h1><?= translateACF('registrations_are_closed'); ?></h1>
        <p>
            <?= translateACF('please_come_back_during_pitch-in-week'); ?>
        </p>
        <div class="row">
            <br>
            <a class='button attend-button'
                href="<?php the_permalink(77); ?>"><?= translateACF('contact_administrator'); ?></a>
        </div>
        <?php else: ?>
        <?php acf_form('register-cleanup'); ?>
        <?php endif; ?>
        <?php endwhile; ?>
    </div>
</main>

<?php get_footer(); ?>