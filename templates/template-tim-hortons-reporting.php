<?php 
// Template Name: Tim Hortons reporting (Current year)
if( current_user_can('editor') || current_user_can('administrator') ) { ?>
    <?php 

	$args = array(
		'post_type' => 'cleanups',
		'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'hide_cleanup',
                'field' => 'slug',
                'terms' => 'yes',
                'operator' => 'NOT IN',
            )
        ),
        'date_query' => array(
            array(
                'year'  => date('Y')
            ),
        ),
    );
    
$cleanups = new WP_Query($args);

// User Loop
if ( $cleanups->have_posts() ) {
    $cleanups_array = [];
    while ( $cleanups->have_posts() ) { $cleanups->the_post();

        $new_args = array_merge(
            $args, array('author__in' => array(get_the_author_meta('ID')))
        );

        $user_cleanup_count = new WP_Query($new_args);

        $cleanup_package = get_field('cleanup_package');

        $num_people = get_field('number_of_participants_break_to_boxes');
        if (!$num_people) {
            $num_people = get_field('number_of_people', $cleanup_package);
        }

        if (!$num_people) {
            $num_people = 0;
        }

        $boost = get_field('participant_boost', get_the_id());

        if (!$boost) {
            $boost = 0;
        }

        $num_volunteers = $num_people + $boost;

        $group_types = get_field('group_type');
        $group_types_string = '';
        foreach($group_types as $group_type) {
            $group_types_string .= $group_type->name . ', ';
        }

        $group_types_string = rtrim($group_types_string, ', ');

        $number_of_bags = get_field('number_of_bags', $cleanup_package);

        // needs event location.
        // $ID = get_the_ID();
        $gmap = get_field('address');
        $cleanups_array[] = [
            'Clean Up Event' => get_the_title(),
            'Organization Name' => get_field('organization_name'),
            'Cleanup Address' => $gmap['address'],
            'City' => get_field('city'),
            'Province' => get_field('provice'),
            'Contact Name' => get_field('first_name') . ' ' . get_field('last_name'),
            'Contact Email Address' => get_the_author_meta('user_email'),
            '# of Clean Ups' => $user_cleanup_count->found_posts,
            '# of Volunteers' => $num_volunteers,
            'Exact Participant count' => get_field('number_of_volunteers'),
            '# of bags' => $number_of_bags,
            'Group type' => $group_types_string,
            'Date' => get_field('date')
        ];

    }
    function generateCsv($data, $filename = 'tim_hortons_cleanup_reporting.csv', $delimiter = ',', $enclosure = '"') {
        $handle = fopen('php://temp', 'r+');
        $headerDisplayed = false;
        foreach ($data as $line) {
                if ( !$headerDisplayed ) {
                    // Use the keys from $data as the titles
                    fputcsv($handle, array_keys($line), $delimiter, $enclosure);
                    $headerDisplayed = true;
                }
                fputcsv($handle, $line, $delimiter, $enclosure);
        }
        // reset the file pointer to the start of the file
        fseek($handle, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($handle);
        // rewind($handle);
        // while (!feof($handle)) {
        //         $contents .= fread($handle, 8192);
        // }
        // fclose($handle);
        // return $contents;
    }
    return generateCsv($cleanups_array);
} else {
	echo 'No cleanups found.';
} 
}
exit();