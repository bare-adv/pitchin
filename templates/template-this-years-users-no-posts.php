<?php 
// Template Name: This years users with no posts
if( current_user_can('editor') || current_user_can('administrator') ) { ?>
    <?php $users = new Wp_User_Query([
        'date_query' => array( 
            array( 'after' => 'January 1st, ' . date('Y') )  
        )
    ]);

// User Loop
if ( ! empty( $users->get_results() ) ) {
    $user_array = [];
	foreach ( $users->get_results() as $user ) {
        $post_count = count_user_posts($user->ID, 'cleanups');
         if($post_count == 0):
             $user_array[] = [
                'name' => $user->data->display_name,
                'email' => $user->data->user_email,
                'date registered' => $user->data->user_registered
            ];
         endif; 
    }

} else {
	echo 'No users found.';
}

function generateCsv($data, $filename = 'users_with_no_cleanups.csv', $delimiter = ',', $enclosure = '"') {
    $handle = fopen('php://temp', 'r+');
    $headerDisplayed = false;
    foreach ($data as $line) {
            if ( !$headerDisplayed ) {
                // Use the keys from $data as the titles
                fputcsv($handle, array_keys($line), $delimiter, $enclosure);
                $headerDisplayed = true;
            }
            fputcsv($handle, $line, $delimiter, $enclosure);
    }
    // reset the file pointer to the start of the file
    fseek($handle, 0);
    // tell the browser it's going to be a csv file
    header('Content-Type: application/csv');
    // tell the browser we want to save it instead of displaying it
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    // make php send the generated csv lines to the browser
    fpassthru($handle);
    // rewind($handle);
    // while (!feof($handle)) {
    //         $contents .= fread($handle, 8192);
    // }
    // fclose($handle);
    // return $contents;
}
 return generateCsv($user_array); 
 
 } 
 exit();