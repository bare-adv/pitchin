<?php
/**
 * The template for displaying the footer.
 */
?>

</section>

<footer>
  <div class="site-info grid">
    &copy; <?php echo date('Y'); ?> PITCH-IN All rights reserved
  </div>

  <div class="style-light footer-scroll-top" style="">
    <a href="#" class="scroll-top">
      <i class="fa fa-angle-up fa-stack fa-rounded btn-default btn-hover-nobg"></i>
    </a>
  </div>
</footer>
<?php wp_footer(); ?>

</body>
</html>
